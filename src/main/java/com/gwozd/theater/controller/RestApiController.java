package com.gwozd.theater.controller;

import com.gwozd.theater.models.Client;
import com.gwozd.theater.models.Show;
import com.gwozd.theater.models.Ticket;
import com.gwozd.theater.services.ClientService;
import com.gwozd.theater.services.SellService;
import com.gwozd.theater.services.ShowService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.Optional;


@RestController
public class RestApiController {

    Logger logger = LoggerFactory.getLogger(RestApiController.class);

    ShowService showService;
    ClientService clientService;
    SellService sellService;

    @Autowired
    public void setShowService(ShowService showService){
        this.showService = showService;
    }

    @Autowired
    public void setClientService(ClientService clientService){
        this.clientService = clientService;
    }

    @Autowired
    public void setSellService(SellService sellService){
        this.sellService = sellService;
    }

    @GetMapping("/api/shows")
    Iterable<Show> showShow(){
        return showService.findAllShows();
    }

    @GetMapping("/api/shows/{id}")
    Optional<Show> showShow(@PathVariable Integer id){
        return showService.findShowById(id);
    }

    @PostMapping("api/shows")
    Optional<Show> addShow(@RequestBody Show show){
        return showService.saveNew(show);
    }

    @DeleteMapping("/api/shows/{id}")
    void delShow(@PathVariable Integer id){
        showService.deleteShowById(id);
    }

//    ------------------------------------------

    @GetMapping("/api/client")
    Iterable<Client> showClient(){
        return clientService.findAllClients();
    }

    @GetMapping("/api/client/{id}")
    Optional<Client> showClient(@PathVariable Integer id){
        return clientService.findClientById(id);
    }

    @PostMapping("api/client")
    Client addClient(@RequestBody Client client){
        return clientService.saveNewClient(client);
    }

    @DeleteMapping("/api/client/{id}")
    void delClient(@PathVariable Integer id){
        clientService.deleteClientById(id);
    }

//    -------------------------

    @PostMapping("api/ticket")
    ResponseEntity buyTicket(@RequestBody Ticket ticket){
        Ticket resp = sellService.buyTicket(ticket);
        if (resp != null) {
            logger.error(resp.toString());
            return ResponseEntity.status(201).body(resp);
        }else{

            return ResponseEntity.status(406).body("Miejsce zajęte!!!!!!!!");
        }

    }

    @GetMapping("api/ticket")
    Iterable<Ticket> showTicket(){
        return sellService.findAllTicket();
    }

    @GetMapping("api/ticket/{id}")
    Optional<Ticket> showTicket(@PathVariable Integer id){
        return sellService.findTicketById(id);
    }

}
