package com.gwozd.theater.controller;

import com.gwozd.theater.models.Client;
import com.gwozd.theater.models.Place;
import com.gwozd.theater.models.Show;
import com.gwozd.theater.models.Ticket;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.RestTemplate;

import java.util.*;
import java.util.stream.Collectors;


@Controller
public class MainController {

    RestTemplate restTemplate = new RestTemplate();
    String url = "http://localhost:8080/api/";


    @GetMapping(value="/")
    public String index(Model model){
        model.addAttribute("name", "Yeeeeah!");

        ResponseEntity<Show[]> shows = restTemplate.getForEntity(url+"shows", Show[].class);
        List<Show> showList = Arrays.stream(shows.getBody()).collect(Collectors.toList());
        model.addAttribute(showList);
        model.addAttribute("showPlaces", "false");
        return "index";
    }

    @PostMapping(value = "/")
    public String show(@RequestParam(name="showId") int showId, Model model){

        ResponseEntity<Show> shows = restTemplate.getForEntity(url+"shows/" + showId, Show.class);
        List<Show> showList = new ArrayList<>();
        showList.add(shows.getBody());

        List<Place> places= showList.get(0).getPlace();

        ResponseEntity<Client[]> client = restTemplate.getForEntity(url+"client", Client[].class);
        List<Client> clientList = Arrays.stream(client.getBody()).collect(Collectors.toList());

        model.addAttribute(showList);
        model.addAttribute("clientList", clientList);
        model.addAttribute("places", places);
        model.addAttribute("showPlaces", "true");

        return "index";
    }

    @PostMapping(value = "/buy")
    public String buy(@RequestParam Map<String,String> allParams, Model model){
        String clientId = allParams.get("client");
        List<String> nameCheckValue = Arrays.asList(allParams.get("nameCheckValue").split(","));


        Ticket ticket = new Ticket();
        Client clientTemp = new Client();
        Place placeTemp = new Place();

        clientTemp.setId(Integer.valueOf(clientId));

        List<Ticket> ticketList = new ArrayList<>();

        for (String i: nameCheckValue) {
            placeTemp.setId(Integer.valueOf(i));
            ticket.setClient(clientTemp);
            ticket.setPlace(placeTemp);
            ResponseEntity<Ticket> resp = restTemplate.postForEntity(url+"ticket/", ticket, Ticket.class);
            System.err.println(resp.getBody().getId());

            ResponseEntity<Ticket> resp2 = restTemplate.getForEntity(url+"ticket/" + resp.getBody().getId(), Ticket.class);

            System.out.println(resp2.getBody().toString());
            ticketList.add(resp2.getBody());
        }


        model.addAttribute("ticketList", ticketList);
        return "ticket";


//        System.out.println(placeId);

    }

}
