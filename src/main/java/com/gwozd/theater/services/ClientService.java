package com.gwozd.theater.services;

import com.gwozd.theater.models.Client;
import com.gwozd.theater.repositories.ClientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ClientService {

    @Autowired
    private ClientRepository clientRepository;

    public Iterable<Client> findAllClients() {
        return clientRepository.findAll();
    }

    public Optional<Client> findClientById(Integer id) {
        return clientRepository.findById(id);
    }

    public Client saveNewClient(Client client) {
        return clientRepository.save(client);
    }

    public void deleteClientById(Integer id) {
        clientRepository.delete(findClientById(id).get());
    }
}
