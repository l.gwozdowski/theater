package com.gwozd.theater.services;

import com.gwozd.theater.models.Place;
import com.gwozd.theater.models.Ticket;
import com.gwozd.theater.repositories.ClientRepository;
import com.gwozd.theater.repositories.PlaceRepository;
import com.gwozd.theater.repositories.TicketRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class SellService {

    @Autowired
    TicketRepository ticketRepository;

    @Autowired
    ClientRepository clientRepository;

    @Autowired
    PlaceRepository placeRepository;

    public Ticket buyTicket(Ticket ticket) {

        Place place = placeRepository.findById(ticket.getPlace().getId()).get();
        if (place.getIsFree()) {
            place.setIsFree(false);
            placeRepository.save(place);
            ticket.getPlace().setIsFree(false);
            return ticketRepository.save(ticket);
        }else{
            return null;
        }
    }

    public Iterable<Ticket> findAllTicket() {
        return ticketRepository.findAll();
    }

    public Optional<Ticket> findTicketById(Integer id){return ticketRepository.findById(id);}
}
