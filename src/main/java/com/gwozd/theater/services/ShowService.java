package com.gwozd.theater.services;

import com.gwozd.theater.models.Place;
import com.gwozd.theater.models.Show;
import com.gwozd.theater.repositories.PlaceRepository;
import com.gwozd.theater.repositories.ShowRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ShowService {

    Logger logger = LoggerFactory.getLogger(ShowService.class);

    @Autowired
    private ShowRepository showRepository;

    @Autowired
    private PlaceRepository placeRepository;

    public Iterable<Show> findAllShows(){
        return showRepository.findAll();
    }

    public Optional<Show> findShowById(Integer id){
        return showRepository.findById(id);
    }

    public Optional<Show> saveNew(Show show){
        logger.info("Orajt: {}", show);
        Show showResult = showRepository.save(show);

        Integer placesToCreate = showResult.getHowManyPlaces();
        List<Place> places = new ArrayList<>();
        for (Integer i = 1; i <= placesToCreate; i++) {
            Place place = new Place();
            place.setIsFree(true);
            place.setPlace(showResult.getId().toString() + "_" + i.toString());
            place.setShow(showResult);
            places.add(place);
        }
        Iterable<Place> pl = placeRepository.saveAll(places);
        logger.info("Teoretycznie dodaliśmy miejsca: {}", pl);
        return showRepository.findById(showResult.getId());
    }

    public void deleteShowById(Integer id) {
        Optional<Show> show = findShowById(id);
        showRepository.delete(show.get());
    }
}
