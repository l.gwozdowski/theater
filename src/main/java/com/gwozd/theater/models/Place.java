package com.gwozd.theater.models;

import lombok.Data;
import javax.persistence.*;

@Data
@Entity
public class Place extends AbstractEntity {

    @Column
    private String place;
    private Boolean isFree;

    @ManyToOne(cascade = CascadeType.ALL)
    private Show show;

}
