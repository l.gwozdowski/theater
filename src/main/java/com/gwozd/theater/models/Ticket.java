package com.gwozd.theater.models;

import lombok.Data;
import javax.persistence.*;

@Data
@Entity
public class Ticket extends AbstractEntity {

    @ManyToOne
    private Place place;

    @ManyToOne
    private Client client;


}
