package com.gwozd.theater.models;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;

@Data
@Entity
public class Client extends AbstractEntity {
    @Column
    private String firstName;
    private String lastName;
}
