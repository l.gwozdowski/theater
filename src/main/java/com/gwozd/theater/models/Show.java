package com.gwozd.theater.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import javax.persistence.*;
import java.time.ZonedDateTime;
import java.util.List;

@Data
@Entity
public class Show extends AbstractEntity {
    @Column
    private String title;
    private ZonedDateTime dateTime;
    private Float price;
    private Integer howManyPlaces;

    @OneToMany(mappedBy = "show",cascade = CascadeType.ALL)
    @JsonIgnoreProperties("show")
    private List<Place> place;

}
