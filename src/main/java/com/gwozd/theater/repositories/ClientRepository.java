package com.gwozd.theater.repositories;

import com.gwozd.theater.models.Client;
import org.springframework.data.repository.CrudRepository;

public interface ClientRepository extends CrudRepository<Client, Integer> {
}
