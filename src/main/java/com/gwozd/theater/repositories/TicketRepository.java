package com.gwozd.theater.repositories;

import com.gwozd.theater.models.Ticket;
import org.springframework.data.repository.CrudRepository;

public interface TicketRepository extends CrudRepository<Ticket, Integer> {
}
