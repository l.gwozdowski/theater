package com.gwozd.theater.repositories;

import com.gwozd.theater.models.Show;
import org.springframework.data.repository.CrudRepository;

public interface ShowRepository extends CrudRepository<Show, Integer> {
}
