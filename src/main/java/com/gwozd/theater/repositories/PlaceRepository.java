package com.gwozd.theater.repositories;

import com.gwozd.theater.models.Place;
import org.springframework.data.repository.CrudRepository;

public interface PlaceRepository extends  CrudRepository<Place, Integer>{
}

